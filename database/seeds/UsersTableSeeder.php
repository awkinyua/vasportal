<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(
            [
                'email' => 'vasdev@safaricom.co.ke'
            ],
            [
                'password' => Hash::make('secret'),
                'name' => 'Vas Portal Admin',
                'role_id' => 1
            ]
        );

        $users = User::all();
        foreach ($users as $user) {
            $role = \Spatie\Permission\Models\Role::find(1);
            $user->assignRole($role);
        }
    }
}
