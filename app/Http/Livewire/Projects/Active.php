<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;
use Livewire\WithPagination;

class Active extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.projects.active', [
            'projects' => \App\Project::latest()->paginate(10)
        ]);
    }
}