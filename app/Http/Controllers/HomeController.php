<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectUpdate;
use App\TeamBoard;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $updates = ProjectUpdate::orderBy('id', 'DESC')->latest()->paginate(10);
        $posts = TeamBoard::orderBy('id', 'DESC')->paginate(10);
        $projects = Project::latest()->paginate(5);
        return view('home', compact('updates', 'posts', 'projects'));
    }
}
