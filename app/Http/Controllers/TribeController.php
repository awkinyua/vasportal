<?php

namespace App\Http\Controllers;

use App\Tribe;
use Illuminate\Http\Request;

class TribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = "Tribes";
        $tribes = Tribe::paginate(10);
        return view('tribes.index', compact('page_name', 'tribes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $user = Tribe::Create(
            [
                'name' => $request->name,
                'description' => $request->description
            ]
        );

        $request->session()->flash('success', 'Tribe account created');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tribe  $tribe
     * @return \Illuminate\Http\Response
     */
    public function show(Tribe $tribe)
    {
        $page_name = $tribe->name . ' Profile';
        return view('tribes.show', compact('tribe', 'page_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tribe  $tribe
     * @return \Illuminate\Http\Response
     */
    public function edit(Tribe $tribe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tribe  $tribe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tribe $tribe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tribe  $tribe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tribe $tribe)
    {
        //
    }
}
