@extends('layouts.app')

@section('content')
    @can('Create posts')

        <div class="x_panel">
            {{ Form::open(['route' => ['board.store']]) }}
            <div class="form-group">
                <label>What's your update</label>
                <textarea class="form-control" name="user_post" required
                          placeholder="What's your update">{!! old('user_post') !!}</textarea>
            </div>
            <div class="form-group">
                <div class="text-right mtop20">
                    <button type="submit" class="btn btn-sm btn-primary">Post
                    </button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    @endcan
    <div class="row mt-4">
        @foreach($posts as $post)
            <div class="col-sm-6 col-xs-12">
                <div class="x_panel">
                    {!! $post->post !!}
                    <hr class="dashed-h"/>
                    <small class="pull-right"> - {!! $post->user->name !!}</small>
                    <small class="pull-left">{!! $post->created_at->diffForHumans() !!}</small>
                </div>
            </div>
        @endforeach
    </div>
@stop
