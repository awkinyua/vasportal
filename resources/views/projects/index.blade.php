@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

                @livewire('projects.search')
            </div>
        </div>
    </div>
@stop

@section('modals')
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add a project</h4>
                </div>
                {{ Form::open(['route' => 'projects.store']) }}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Project name<span class="required">*</span></label>
                        {!!  Form::text('name', $value = null, $attributes =[
                        'class' => 'form-control','placeholder' => 'Project name'
                        ]); !!}
                    </div>
                    <div class="form-group">
                        <label>Project description<span class="required">*</span></label>
                        <textarea class="form-control" name="description" placeholder="Project description"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Project</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{!! asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <script>
        $(document).ready(function () {
            $(".delete-client").on('click', function () {
                var r = confirm("Are you sure you want to delete this project?");
                if (r) {
                    $.ajax({
                        url: '{!! url('projects') !!}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting project");
                        }
                    });
                }
            });

        });
    </script>
@endsection
