@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="text-left pull-left mtop20">
                <a href="{!! URL::to( 'projects/') !!}"
                    class="btn btn-md btn-primary">All Projects</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="x_panel">
                <div class="text-left pull-left mtop20">
                    <a href="{!! URL::to( 'projects/' . $previous ) !!}"
                class="btn btn-sm btn-default {{ $previous == null ? 'disabled' : ''}}">&laquo; Previous Project</a>
                </div>
                <div class="text-right pull-right mtop20">
                    <a href="{!! URL::to( 'projects/' . $next ) !!}"
                        class="btn btn-sm btn-default {{ $next == null ? 'disabled' : ''}}">Next Project &raquo;</a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Project Description</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4 class="green">
                        {!! $project->name !!}
                        {!! $project->status_badge !!}
                    </h4>
                    <p>{!! $project->completion_level !!}% complete</p>
                    <div class="progress progress_sm">
                        <div class="progress-bar bg-green" role="progressbar"
                             data-transitiongoal="{!! $project->completion_level !!}"
                             aria-valuenow="49" style="width: {!! $project->completion_level !!}%;"></div>
                    </div>
                    <br/>
                    <p>
                        {!! $project->description !!}
                    </p>
                    <p>
                        {!! $project->tribe !!}
                    </p>
                    <p>
                        {!! $project->squad !!}
                    </p>
                    <br>


                    @can('Join projects')
                        <br> @if(!$project->im_member)
                            <div class="text-right mtop20">
                                <a href="{!! route('projects.join',[Auth::user()->id, $project->id]) !!}"
                                   class="btn btn-sm btn-primary">Join project</a>
                            </div>
                        @else
                            <div class="text-right mtop20">
                                <a href="{!! route('projects.leave',[Auth::user()->id, $project->id]) !!}"
                                   class="btn btn-sm btn-danger">Leave project</a>
                            </div>
                        @endif
                    @endcan
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                   aria-expanded="true">Timeline</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content2" role="tab" id="profile-tab3"
                                   data-toggle="tab" aria-expanded="false">Members</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content3" role="tab" id="profile-tab2"
                                   data-toggle="tab" aria-expanded="false">Profile</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">
                                @if($project->im_member)
                                    <div class="row">
                                        <div class="col-12">
                                            {{ Form::model($project,['route' => ['projects.update.save', $project->id]]) }}
                                            <div class="form-group row">
                                                <div class="col-sm-6">
                                                    <label>Title</label>
                                                    <input type="text" class="form-control" placeholder="Update title"
                                                           value="{!! old('title', $project->title) !!}"
                                                           name="title" required/>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Date</label>
                                                    <input type="text" name="datetime" class="form-control"/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Completion %ge</label>
                                                    <input type="number" min="{!! $project->completion_level !!}"
                                                           max="100" class="form-control"
                                                           placeholder="How far are we here?"
                                                           value="{!! old('completion_level', $project->completion_level) !!}"
                                                           name="completion_level" required/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>What's your update</label>
                                                <textarea class="form-control" name="description" required
                                                          placeholder="What's your update">{!! old('description') !!}</textarea>
                                            </div>
                                            <div class="form-group">
                                            </div>
                                            <div class="form-group">
                                                <div class="text-right mtop20">
                                                    <button type="submit" class="btn btn-sm btn-primary">Post
                                                    </button>
                                                </div>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                @endif

                                <h4>Recent Activity</h4>
                                <ul class="messages">
                                    @foreach($project->updates()->orderBy('id','desc')->get() as $update) 
                                        <li>
                                            <div class="message_date">
                                                <h4 class="date text-info">{!! $update->completion_level !!}%</h4>
                                                <p class="month">{!! $update->created_at->diffForHumans() !!}</p>
                                            </div>
                                            <div class="message_wrapper">

                                                <h5><strong>{!! $update->title !!}</strong>
                                                    <small>
                                                        - {!! $update->user->link !!}
                                                    </small>
                                                </h5>
                                                <h6>
                                                    <small>{!! $update->start_date->toRfc7231String() !!} to {!! $update->start_date->toRfc7231String() !!}</small>
                                                </h6>
                                                <p class="message">
							{!! \Michelf\Markdown::defaultTransform($update->description) !!}
                                                </p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                 aria-labelledby="home-tab">
                                <table class="table table-striped table-sm no-margin">
                                    <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>
                                            Name
                                        </th>
                                        <th>Email</th>
                                        <th>Projects</th>
                                        <th>Last connected</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($project->members as $user)
                                        <tr>
                                            <td>{!! $user->id !!}</td>
                                            <td>{!! $user->link !!}</td>
                                            <td>{!! $user->email !!}</td>
                                            <td>0</td>
                                            <td>{!! $user->updated_at->diffForHumans() !!}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                                 aria-labelledby="profile-tab">
                                {{ Form::model($project,['route' => ['projects.update', $project->id],'method' => 'put']) }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Role name<span class="required">*</span></label>
                                        {!!  Form::text('name', $project->name, $attributes =[
                                        'class' => 'form-control'
                                        ]); !!}
                                    </div>
                                    <div class="form-group">
                                        <label>Role description<span class="required">*</span></label>
                                        <textarea class="form-control" name="description"
                                                  placeholder="Role description">{!! $project->description !!}</textarea>
                                    </div>
                                </div>

                                @can('Edit projects')
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endcan
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="x_panel">
            <div class="text-left pull-left mtop20">
                <a href="{!! URL::to( 'projects/' . $previous ) !!}"
            class="btn btn-sm btn-default {{ $previous == null ? 'disabled' : ''}}">&laquo; Previous Project</a>
            </div>
            <div class="text-right pull-right mtop20">
                <a href="{!! URL::to( 'projects/' . $next ) !!}"
                    class="btn btn-sm btn-default {{ $next == null ? 'disabled' : ''}}">Next Project &raquo;</a>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{!! asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <script>
    $(function() {
        $('input[name="datetime"]').daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        Z:"+03:00",
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
        });
    });
    </script>
@stop
