<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
    <div class="count">{!! \App\User::count() !!}</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i>Total Projects</span>
    <div class="count">{!! \App\Project::count() !!}</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i>Ongoing Projects</span>
    <div class="count">{!! \App\Project::count() !!}</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Updates this week</span>
    <div class="count">{!! \App\ProjectUpdate::where('created_at',">=", \Carbon\Carbon::now()->startOfWeek())->count() !!}</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Core Systems</span>
    <div class="count">{!! \App\System::count() !!}</div>
</div>
