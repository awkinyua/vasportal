<div class="table-responsive">
    <table class="table table-striped table-sm  no-margin">
        <thead>
        <tr>
            <th>Name</th>
            <th>Project progress</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{!! $project->link !!}</td>
                <td>
                    <div class="progress progress_sm">
                        <div class="progress-bar bg-green" role="progressbar"
                                data-transitiongoal="{!! $project->completion_level !!}"
                                aria-valuenow="49"
                                style="width: {!! $project->completion_level !!}%;"></div>
                    </div>
                </td>
                <td>{!! $project->status_badge !!}</td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
    @if($projects->first())
        <div class="col">
            &nbsp &nbsp {{ $projects->links() }}
        </div>
        <div class="col text-right text-muted">
            showing {{ $projects->firstItem() }} to {{ $projects->lastItem() }} out of {{ $projects->total() }} Projects &nbsp &nbsp
        </div>
    @endif
    </div>
</div>