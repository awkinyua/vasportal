<div>
    <ul class="list-unstyled timeline widget">
        @foreach($posts as $post)
            <li>
                <div class="block">
                    <div class="block_content">
                        <p class="excerpt">{!! $post->post !!}</p>
                        <div class="byline">
                            <span>{!! $post->created_at->diffForHumans() !!}</span> by
                            <a>{!! $post->user->name !!}</a>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>
